import chalk from 'chalk';
import fs from 'fs';
import ncp from 'ncp';
import path from 'path';
import { promisify } from 'util';
const fetch = require("node-fetch");

const access = promisify(fs.access);
const copy = promisify(ncp);

async function copyTemplateFiles(options) {
    return copy(options.templateDirectory, options.targetDirectory, {
        clobber: false,
    });
}

/* this async func will actually hit the API for the items needed */
export async function createProject(options) {
    options = {
        ...options,
        targetDirectory: options.targetDirectory || process.cwd(),
    };

    if(options.host)
    {
        console.log("host is : " + options.host);
        console.log("querying username: " + options.un);
        console.log("connecting...");
        console.log();
    }
    (async () => {
        //pass host and query as formatted string
        if(options.query === 'userfeed' || options.query === 'retest')
        {
            const response = await fetch('http://'+options.host+':6543/'+options.query+'/');
            const body = await response.json();
            console.log(JSON.stringify(body, null, 4));
        }
        if(options.query === '--docker')
        {
            console.log("will read dockerfile");

            //do more here
        }
        else
        {
            const response = await fetch('http://'+options.host+':6543/' + options.query + '/' + options.un);
            const body = await response.json();
            console.log(JSON.stringify(body, null, 4));
        }



        })();

}
