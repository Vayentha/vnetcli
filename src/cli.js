import arg from 'arg';
import inquirer from 'inquirer';
import { createProject } from "./main";

function parseArgumentsIntoOptions(rawArgs)
{
    const args = arg(

        {
            '--query': String,
            '--uname': String,
            '--host': String,
            '-h': '--host',
        },

        {
            argv: rawArgs.slice(2),
        }
    );
    return {
        host: args['--host'] || false,
        un: args['--uname'] || false,
        query: args['--query'] || false,
    };
}


async function promptForMissingOptions(options) {
    const defaultTemplate = 'JavaScript';
    if (options.skipPrompts) {
        return {
            ...options,
            template: options.template || defaultTemplate,
        };
    }

    const questions = [];
    if (!options.host) {
        questions.push({
            type: 'list',
            name: 'host',
            message: 'choose a backend',
            choices: ['10.0.0.21', '127.0.0.1'],
            default: defaultTemplate,
        });
    }

    if (!options.un) {
        questions.push({
            type: 'input',
            name: 'un',
            message: 'choose a username',
            default: 'username',
        });
    }

    if (!options.query)
    {
        questions.push({
            type: 'list',
            name: 'query',
            message: 'choose from the possible queries',
            choices: ['amounts', 'bigpost', 'creds', 'userfeed', 'retest', '--docker'],
            default: defaultTemplate,
        });
    }



    const answers = await inquirer.prompt(questions);
    return {
        ...options,
        host: answers.host,
        un: answers.un,
        query: answers.query,
    };
}




export async function cli(args)
{
    let options = parseArgumentsIntoOptions(args);
    options = await promptForMissingOptions(options);
    //console.log(options);
    await createProject(options);
}